package ru.tsc.kirillov.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kirillov.tm.enumerated.Role;

import java.io.ObjectOutputStream;
import java.nio.file.Files;

public final class DataSaveBackupCommand extends AbstractDataCommand {

    @NotNull
    public static final String NAME = "backup-save";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Сохранить бэкап состояния приложения";
    }

    @Override
    @SneakyThrows
    public void execute() {
        try (@NotNull final ObjectOutputStream objectOutputStream =
                     new ObjectOutputStream(Files.newOutputStream(getPathFile(FILE_BACkUP)))) {
            objectOutputStream.writeObject(getDomain());
        }
    }

    @Nullable
    @Override
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

}
