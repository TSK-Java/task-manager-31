package ru.tsc.kirillov.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kirillov.tm.dto.Domain;
import ru.tsc.kirillov.tm.enumerated.Role;
import sun.misc.BASE64Decoder;

import java.io.ByteArrayInputStream;
import java.io.ObjectInputStream;
import java.nio.file.Files;

public final class DataLoadBase64Command extends AbstractDataCommand {

    @NotNull
    public static final String NAME = "data-load-base64";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Загрузить состояние приложения в формате Base64";
    }

    @Override
    @SneakyThrows
    public void execute() {
        System.out.println("[Загрузить состояния приложения в формате Base64]");
        @NotNull final byte[] base64Byte = Files.readAllBytes(getPathFile(FILE_BASE64));
        @Nullable final String base64 = new String(base64Byte);
        @Nullable final byte[] buffer = new BASE64Decoder().decodeBuffer(base64);
        try (@NotNull final ObjectInputStream objectInputStream =
                     new ObjectInputStream(new ByteArrayInputStream(buffer))) {
            @NotNull Domain domain = (Domain) objectInputStream.readObject();
            setDomain(domain);
        }
    }

    @Nullable
    @Override
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

}
