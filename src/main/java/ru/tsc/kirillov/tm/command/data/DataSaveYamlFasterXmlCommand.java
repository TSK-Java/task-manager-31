package ru.tsc.kirillov.tm.command.data;

import com.fasterxml.jackson.dataformat.yaml.YAMLMapper;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kirillov.tm.dto.Domain;
import ru.tsc.kirillov.tm.enumerated.Role;

import java.io.OutputStream;
import java.nio.file.Files;

public final class DataSaveYamlFasterXmlCommand extends AbstractDataCommand {

    @NotNull
    @Override
    public String getName() {
        return "data-save-yaml-fasterxml";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Сохранить состояние приложения из yaml файла (FasterXML API)";
    }

    @Override
    @SneakyThrows
    public void execute() {
        System.out.println("[Сохранение состояния приложения из yaml файла (FasterXML API)]");
        try (@NotNull final OutputStream outputStream =
                     Files.newOutputStream(getPathFile(FILE_FASTERXML_YAML))) {
            @NotNull final Domain domain = getDomain();
            @NotNull final YAMLMapper yamlMapper = new YAMLMapper();
            @NotNull final String yaml = yamlMapper.writerWithDefaultPrettyPrinter().writeValueAsString(domain);
            outputStream.write(yaml.getBytes());
        }
    }

    @Nullable
    @Override
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

}
