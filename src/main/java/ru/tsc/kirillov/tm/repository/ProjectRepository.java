package ru.tsc.kirillov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kirillov.tm.api.repository.IProjectRepository;
import ru.tsc.kirillov.tm.model.AbstractModel;
import ru.tsc.kirillov.tm.model.Project;

public class ProjectRepository extends AbstractUserOwnedRepository<Project> implements IProjectRepository {

    @NotNull
    @Override
    public Project create(@Nullable final String userId, @NotNull final String name) {
        final Project project = new Project();
        project.setName(name);
        project.setUserId(userId);
        return add(project);
    }

    @NotNull
    @Override
    public Project create(@Nullable final String userId, @NotNull final String name, @NotNull final String description) {
        final Project project = new Project();
        project.setName(name);
        project.setUserId(userId);
        project.setDescription(description);
        return add(project);
    }

    @NotNull
    @Override
    public String[] findAllId(@Nullable final String userId) {
        return findAll(userId)
                .stream()
                .map(AbstractModel::getId)
                .toArray(String[]::new);
    }

}
