package ru.tsc.kirillov.tm.command.data;

import com.fasterxml.jackson.dataformat.yaml.YAMLMapper;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kirillov.tm.dto.Domain;
import ru.tsc.kirillov.tm.enumerated.Role;

import java.io.InputStream;
import java.nio.file.Files;

public final class DataLoadYamlFasterXmlCommand extends AbstractDataCommand {

    @NotNull
    @Override
    public String getName() {
        return "data-load-yaml-fasterxml";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Загрузить состояние приложения из yaml файла (FasterXML API)";
    }

    @Override
    @SneakyThrows
    public void execute() {
        System.out.println("[Загрузка состояния приложения из yaml файла (FasterXML API)]");
        try (@NotNull final InputStream inputStream =
                     Files.newInputStream(getPathFile(FILE_FASTERXML_YAML))) {
            @NotNull final YAMLMapper yamlMapper = new YAMLMapper();
            @NotNull final Domain domain = yamlMapper.readValue(inputStream, Domain.class);
            setDomain(domain);
        }
    }

    @Nullable
    @Override
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

}
