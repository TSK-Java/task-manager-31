package ru.tsc.kirillov.tm.exception.field;

public final class StatusEmptyException extends AbstractFieldException {

    public StatusEmptyException() {
        super("Ошибка! Статус не задан.");
    }

}
