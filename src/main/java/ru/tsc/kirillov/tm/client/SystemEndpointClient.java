package ru.tsc.kirillov.tm.client;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kirillov.tm.dto.request.ServerAboutRequest;
import ru.tsc.kirillov.tm.dto.request.ServerVersionRequest;
import ru.tsc.kirillov.tm.dto.response.ServerAboutResponse;
import ru.tsc.kirillov.tm.dto.response.ServerVersionResponse;

public final class SystemEndpointClient extends AbstractEndpoint {

    @NotNull
    @Override
    public ServerAboutResponse getAbout(@NotNull final ServerAboutRequest request) {
        return (ServerAboutResponse) call(request);
    }

    @NotNull
    @Override
    public ServerVersionResponse getVersion(@NotNull final ServerVersionRequest request) {
        return (ServerVersionResponse) call(request);
    }

    public static void main(@Nullable final  String[] args) {
        @NotNull final SystemEndpointClient client = new SystemEndpointClient();
        client.connect();
        @NotNull final ServerAboutResponse serverAboutResponse = client.getAbout(new ServerAboutRequest());
        System.out.println(serverAboutResponse.getName());
        System.out.println(serverAboutResponse.getEmail());
        @NotNull final ServerVersionResponse serverVersionResponse = client.getVersion(new ServerVersionRequest());
        System.out.println(serverVersionResponse.getVersion());
        client.disconnect();
    }

}
