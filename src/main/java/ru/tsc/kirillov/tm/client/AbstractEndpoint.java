package ru.tsc.kirillov.tm.client;

import lombok.Getter;
import lombok.Setter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.tsc.kirillov.tm.api.endpoint.ISystemEndpoint;

import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.net.Socket;

@Getter
@Setter
public abstract class AbstractEndpoint implements ISystemEndpoint {

    @NotNull
    private String host = "localhost";

    @NotNull
    private Integer port = 6060;

    @NotNull Socket socket;

    @NotNull
    @SneakyThrows
    private OutputStream getOutputStream() {
        return socket.getOutputStream();
    }

    @NotNull
    @SneakyThrows
    private ObjectOutputStream getObjectOutputStream() {
        return new ObjectOutputStream(getOutputStream());
    }

    @NotNull
    @SneakyThrows
    private InputStream getInputStream() {
        return socket.getInputStream();
    }

    @NotNull
    @SneakyThrows
    private ObjectInputStream getObjectInputStream() {
        return new ObjectInputStream(getInputStream());
    }

    @NotNull
    @SneakyThrows
    protected Object call(@NotNull final Object data) {
        getObjectOutputStream().writeObject(data);
        return getObjectInputStream().readObject();
    }

    @SneakyThrows
    public void connect() {
        socket = new Socket(host, port);
    }

    @SneakyThrows
    public void disconnect() {
        socket.close();
    }

}
