package ru.tsc.kirillov.tm.component;

import org.jetbrains.annotations.NotNull;
import ru.tsc.kirillov.tm.command.data.AbstractDataCommand;
import ru.tsc.kirillov.tm.command.data.DataLoadBackupCommand;
import ru.tsc.kirillov.tm.command.data.DataSaveBackupCommand;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public final class Backup {

    @NotNull
    private final Bootstrap bootstrap;

    @NotNull
    private final ScheduledExecutorService es = Executors.newSingleThreadScheduledExecutor();

    public Backup(@NotNull final Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
    }

    public void start() {
        load();
        es.scheduleWithFixedDelay(this::save, 0, 3, TimeUnit.SECONDS);
    }

    public void stop() {
        es.shutdown();
    }

    public void save() {
        bootstrap.processCommand(DataSaveBackupCommand.NAME, false);
    }

    public void load() {
        if (AbstractDataCommand.isFileExists(AbstractDataCommand.FILE_BACkUP))
            bootstrap.processCommand(DataLoadBackupCommand.NAME, false);
    }

}
