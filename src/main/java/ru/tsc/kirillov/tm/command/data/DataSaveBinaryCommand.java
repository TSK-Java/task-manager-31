package ru.tsc.kirillov.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kirillov.tm.enumerated.Role;

import java.io.ObjectOutputStream;
import java.nio.file.Files;

public final class DataSaveBinaryCommand extends AbstractDataCommand {

    @NotNull
    @Override
    public String getName() {
        return "data-save-bin";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Сохранить состояние приложения в бинарном формате";
    }

    @Override
    @SneakyThrows
    public void execute() {
        System.out.println("[Сохранение состояния приложения в бинарном формате]");
        try (@NotNull final ObjectOutputStream objectOutputStream =
                     new ObjectOutputStream(Files.newOutputStream(getPathFile(FILE_BINARY)))) {
            objectOutputStream.writeObject(getDomain());
        }
    }

    @Nullable
    @Override
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

}
