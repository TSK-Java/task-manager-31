package ru.tsc.kirillov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kirillov.tm.api.repository.IUserRepository;
import ru.tsc.kirillov.tm.model.User;

import java.util.Optional;
import java.util.function.Predicate;

public class UserRepository extends AbstractRepository<User> implements IUserRepository {

    @NotNull
    protected Predicate<User> predicateByLogin(@NotNull final String login) {
        return user -> login.equals(user.getLogin());
    }

    @NotNull
    protected Predicate<User> predicateByEmail(@NotNull final String email) {
        return user -> email.equals(user.getLogin());
    }

    @Nullable
    @Override
    public User findByLogin(@NotNull final String login) {
        return models
                .stream()
                .filter(predicateByLogin(login))
                .findFirst()
                .orElse(null);
    }

    @Nullable
    @Override
    public User findByEmail(@NotNull final String email) {
        return models
                .stream()
                .filter(predicateByEmail(email))
                .findFirst()
                .orElse(null);
    }

    @Nullable
    @Override
    public User removeByLogin(@NotNull final String login) {
        Optional<User> user = Optional.ofNullable(findByLogin(login));
        user.ifPresent(this::remove);
        return user.orElse(null);
    }

}
