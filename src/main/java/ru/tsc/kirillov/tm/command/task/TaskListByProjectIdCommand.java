package ru.tsc.kirillov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.tsc.kirillov.tm.util.TerminalUtil;

public final class TaskListByProjectIdCommand extends AbstractTaskListCommand {

    @NotNull
    @Override
    public String getName() {
        return "task-show-by-project-id";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Отобразить список задач проекта.";
    }

    @Override
    public void execute() {
        System.out.println("[Список задач проекта]");
        System.out.println("Введите ID проекта:");
        @NotNull final String projectId = TerminalUtil.nextLine();
        printTask(getTaskService().findAllByProjectId(getUserId(), projectId));
    }

}
