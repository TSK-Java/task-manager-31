package ru.tsc.kirillov.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kirillov.tm.command.AbstractCommand;
import ru.tsc.kirillov.tm.dto.Domain;
import ru.tsc.kirillov.tm.exception.system.CommandNotRegisteredException;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public abstract class AbstractDataCommand extends AbstractCommand {

    @NotNull
    protected static final String CATALOG_DATA = "dump";

    @NotNull
    public static final String FILE_BINARY = "data.bin";

    @NotNull
    public static final String FILE_BACkUP = "backup.bin";

    @NotNull
    public static final String FILE_BASE64 = "data.base64";

    @NotNull
    public static final String FILE_JAXB_XML = "data.jaxb.xml";

    @NotNull
    public static final String FILE_JAXB_JSON = "data.jaxb.json";

    @NotNull
    public static final String FILE_FASTERXML_XML = "data.fasterxml.xml";

    @NotNull
    public static final String FILE_FASTERXML_JSON = "data.fasterxml.json";

    @NotNull
    public static final String FILE_FASTERXML_YAML = "data.fasterxml.yaml";

    @NotNull
    public static final String JAVAX_XML_BIND_CONTEXT_FACTORY = "javax.xml.bind.context.factory";

    @NotNull
    public static final String ORG_ECLIPSE_PERSISTENCE_JAXB_JAXBCONTEXT_FACTORY = "org.eclipse.persistence.jaxb.JAXBContextFactory";

    @NotNull
    public static final String APPLICATION_JSON = "application/json";

    @NotNull
    protected Domain getDomain() {
        if (serviceLocator == null) throw new CommandNotRegisteredException();
        @NotNull Domain domain = new Domain();
        domain.setProjects(serviceLocator.getProjectService().findAll());
        domain.setTasks(serviceLocator.getTaskService().findAll());
        domain.setUsers(serviceLocator.getUserService().findAll());
        return domain;
    }

    protected void setDomain(@Nullable final Domain domain) {
        if (domain == null) return;
        if (serviceLocator == null) throw new CommandNotRegisteredException();
        serviceLocator.getProjectService().set(domain.getProjects());
        serviceLocator.getTaskService().set(domain.getTasks());
        serviceLocator.getUserService().set(domain.getUsers());
        serviceLocator.getAuthService().logout();
    }

    @NotNull
    @SneakyThrows
    protected static Path getPathFile(@NotNull String fileName) {
        @NotNull final Path pathCatalog = Paths.get("./", CATALOG_DATA);
        @NotNull final Path pathFile = Paths.get("./", CATALOG_DATA, fileName);
        if (!Files.exists(pathCatalog)) Files.createDirectory(pathCatalog);
        return pathFile;
    }

    public static boolean isFileExists(@NotNull String fileName) {
        @NotNull Path path = getPathFile(fileName);
        return Files.exists(path);
    }

}
