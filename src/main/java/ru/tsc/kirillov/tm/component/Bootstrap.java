package ru.tsc.kirillov.tm.component;

import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.reflections.Reflections;
import ru.tsc.kirillov.tm.api.endpoint.ISystemEndpoint;
import ru.tsc.kirillov.tm.api.repository.ICommandRepository;
import ru.tsc.kirillov.tm.api.repository.IProjectRepository;
import ru.tsc.kirillov.tm.api.repository.ITaskRepository;
import ru.tsc.kirillov.tm.api.repository.IUserRepository;
import ru.tsc.kirillov.tm.api.service.*;
import ru.tsc.kirillov.tm.command.AbstractCommand;
import ru.tsc.kirillov.tm.dto.request.ServerAboutRequest;
import ru.tsc.kirillov.tm.dto.request.ServerVersionRequest;
import ru.tsc.kirillov.tm.endpoint.SystemEndpoint;
import ru.tsc.kirillov.tm.exception.system.ArgumentNotSupportedException;
import ru.tsc.kirillov.tm.exception.system.CommandNotSupportedException;
import ru.tsc.kirillov.tm.repository.CommandRepository;
import ru.tsc.kirillov.tm.repository.ProjectRepository;
import ru.tsc.kirillov.tm.repository.TaskRepository;
import ru.tsc.kirillov.tm.repository.UserRepository;
import ru.tsc.kirillov.tm.service.*;
import ru.tsc.kirillov.tm.util.SystemUtil;
import ru.tsc.kirillov.tm.util.TerminalUtil;

import java.io.File;
import java.lang.reflect.Modifier;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Set;

public final class Bootstrap implements IServiceLocator {

    @NotNull
    private final ICommandRepository commandRepository = new CommandRepository();

    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository();

    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository();

    @NotNull
    private final IUserRepository userRepository = new UserRepository();

    @Getter
    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @Getter
    @NotNull
    private final ICommandService commandService = new CommandService(commandRepository);

    @Getter
    @NotNull
    private final ITaskService taskService = new TaskService(taskRepository);

    @Getter
    @NotNull
    private final IProjectService projectService = new ProjectService(projectRepository);

    @Getter
    @NotNull
    private final IProjectTaskService projectTaskService =  new ProjectTaskService(projectRepository, taskRepository);

    @Getter
    @NotNull
    private final ILoggerService loggerService = new LoggerService();

    @NotNull
    private final FileCommand fileCommand = new FileCommand(this);

    @Getter
    @NotNull
    private final IUserService userService = new UserService(
            propertyService,
            userRepository,
            taskRepository,
            projectRepository
    );

    @Getter
    @NotNull
    private final IAuthService authService = new AuthService(propertyService, userService);

    @NotNull
    private final ISystemEndpoint systemEndpoint = new SystemEndpoint(this);

    @NotNull
    private final Backup backup = new Backup(this);

    @NotNull
    private final Server server = new Server(this);

    {
        @NotNull final String packageCommands = AbstractCommand.class.getPackage().getName();
        @NotNull final Reflections reflections = new Reflections(packageCommands);
        @NotNull final Set<Class<? extends AbstractCommand>> classes =
                reflections.getSubTypesOf(AbstractCommand.class);
        for (@NotNull final Class clazz: classes) {
            registry(clazz);
        }
    }

    {
        server.registry(ServerAboutRequest.class, systemEndpoint::getAbout);
        server.registry(ServerVersionRequest.class, systemEndpoint::getVersion);
    }

    @SneakyThrows
    private void registry(@NotNull final Class clazz) {
        if (Modifier.isAbstract(clazz.getModifiers())) return;
        if (!AbstractCommand.class.isAssignableFrom(clazz)) return;
        registry((AbstractCommand) clazz.newInstance());
    }

    private void registry(@NotNull final AbstractCommand command) {
        command.setServiceLocator(this);
        commandService.add(command);
    }

    private void prepareStartup() {
        loggerService.info("** Добро пожаловать в Task Manager **");
        Runtime.getRuntime().addShutdownHook(new Thread(this::prepareShutdown));
        initPID();
        backup.start();
        fileCommand.start();
        server.start();
    }

    private void prepareShutdown() {
        loggerService.info("** Task Manager завершил свою работу **");
        fileCommand.stop();
        backup.stop();
        server.stop();
    }

    @SneakyThrows
    private void initPID() {
        @NotNull final String fileName = "task-manager.pid";
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        Files.write(Paths.get(fileName), pid.getBytes(StandardCharsets.UTF_8));
        @NotNull final File file = new File(fileName);
        file.deleteOnExit();
    }

    private boolean processArgument(@Nullable final String[] args) {
        if (args == null || args.length == 0) return false;
        @Nullable final String firstArg = args[0];
        processArgument(firstArg);
        return true;
    }

    public void processCommand(@Nullable final String command) {
        processCommand(command, true);
    }

    public void processCommand(@Nullable final String command, boolean checkRoles) {
        @Nullable final AbstractCommand abstractCommand = commandService.getCommandByName(command);
        if (abstractCommand == null)
            throw new CommandNotSupportedException(command);
        if (checkRoles) authService.checkRoles(abstractCommand.getRoles());
        abstractCommand.execute();
    }

    private void processArgument(@Nullable final String argument) {
        @Nullable final AbstractCommand abstractCommand = commandService.getCommandByArgument(argument);
        if (abstractCommand == null)
            throw new ArgumentNotSupportedException(argument);
        abstractCommand.execute();
    }

    private void close() {
        System.exit(0);
    }

    private void processCommand() {
        try {
            System.out.println("\nВведите команду:");
            @NotNull final String cmdText = TerminalUtil.nextLine();
            processCommand(cmdText);
            System.out.println("[Ок]");
            loggerService.command(cmdText);
        } catch (@NotNull final Exception e) {
            loggerService.error(e);
            System.err.println("[Ошибка]");
        }
    }

    public void run(@Nullable final String[] args) {
        if (processArgument(args)) close();
        prepareStartup();
        while (true) processCommand();
    }

}
