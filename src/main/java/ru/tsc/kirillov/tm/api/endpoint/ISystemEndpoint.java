package ru.tsc.kirillov.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.tsc.kirillov.tm.dto.request.ServerAboutRequest;
import ru.tsc.kirillov.tm.dto.request.ServerVersionRequest;
import ru.tsc.kirillov.tm.dto.response.ServerAboutResponse;
import ru.tsc.kirillov.tm.dto.response.ServerVersionResponse;

public interface ISystemEndpoint {

    @NotNull
    ServerAboutResponse getAbout(@NotNull ServerAboutRequest request);

    @NotNull
    ServerVersionResponse getVersion(@NotNull ServerVersionRequest request);

}
