package ru.tsc.kirillov.tm.command.data;

import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kirillov.tm.dto.Domain;
import ru.tsc.kirillov.tm.enumerated.Role;

import java.io.OutputStream;
import java.nio.file.Files;

public final class DataSaveXmlFasterXmlCommand extends AbstractDataCommand {

    @NotNull
    @Override
    public String getName() {
        return "data-save-xml-fasterxml";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Сохранить состояние приложения из xml файла (FasterXML API)";
    }

    @Override
    @SneakyThrows
    public void execute() {
        System.out.println("[Сохранение состояния приложения из xml файла (FasterXML API)]");
        try (@NotNull final OutputStream outputStream =
                     Files.newOutputStream(getPathFile(FILE_FASTERXML_XML))) {
            @NotNull final Domain domain = getDomain();
            @NotNull final XmlMapper xmlMapper = new XmlMapper();
            @NotNull final String xml = xmlMapper.writerWithDefaultPrettyPrinter().writeValueAsString(domain);
            outputStream.write(xml.getBytes());
        }
    }

    @Nullable
    @Override
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

}
