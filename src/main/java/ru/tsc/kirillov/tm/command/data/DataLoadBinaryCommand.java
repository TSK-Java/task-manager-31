package ru.tsc.kirillov.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kirillov.tm.dto.Domain;
import ru.tsc.kirillov.tm.enumerated.Role;

import java.io.ObjectInputStream;
import java.nio.file.Files;

public final class DataLoadBinaryCommand extends AbstractDataCommand {

    @NotNull
    public static final String NAME = "data-load-bin";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Загрузить состояние приложения в бинарном формате";
    }

    @Override
    @SneakyThrows
    public void execute() {
        System.out.println("[Загрузка состояния приложения в бинарном формате]");
        try (@NotNull final ObjectInputStream objectInputStream =
                     new ObjectInputStream(Files.newInputStream(getPathFile(FILE_BINARY)))) {
            @NotNull final Domain domain = (Domain) objectInputStream.readObject();
            setDomain(domain);
        }
    }

    @Nullable
    @Override
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

}
